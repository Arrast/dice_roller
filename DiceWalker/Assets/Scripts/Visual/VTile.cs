﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VTile : MonoBehaviour
{
    public class Config 
    {
        public int rotation;
    }

    [SerializeField] private GameObject walls;

    [ExecuteInEditMode]
    public void Init(int rotation)
    {
        if (walls != null) 
        {
            walls.transform.localRotation = Quaternion.Euler(0,0,rotation);
        }
    }
}
