﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TileType = Tile.TileType;
public class Map
{
    public const int UP = 0;
    public const int RIGHT = 1;
    public const int DOWN = 2;
    public const int LEFT = 3;

    // Start is called before the first frame update
    public Tile[,] tiles;
    public int rows;
    public int columns;
    public Pair<int, int> startTilePosition;

    public Map(int rows, int columns)
    {
        this.rows = rows;
        this.columns = columns;
        tiles = new Tile[rows, columns];

        // Initialize
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                tiles[i, j] = new Tile();
                tiles[i, j].row = i;
                tiles[i, j].column = j;
            }
        }
    }

    public Pair<string, int> GetTileIdByTileType(int row, int column, TileType tileType)
    {
        string cellId = tileType.ToString();
        if (tileType == TileType.Wall)
        {
            return GetPositionAndRotationForWall(row, column);
        }

        return new Pair<string, int>(cellId, 0);
    }

    private Pair<string, int> GetPositionAndRotationForWall(int row, int column)
    {
        string cellId = "Wall";
        int rotation = 0;
        int wallCount = 0;

        List<Tile> adjacentTiles = new List<Tile>();
        // Check number of Corners
        if (row > 0 && tiles[row - 1, column].tileType == TileType.Wall)
        {
            adjacentTiles.Add(tiles[row - 1, column]);
            wallCount++;
        }
        else 
        {
            adjacentTiles.Add(null);
        }

        if (column < tiles.GetLength(1) - 1 && tiles[row, column + 1].tileType == TileType.Wall)
        {
            adjacentTiles.Add(tiles[row, column + 1]);
            wallCount++;
        }
        else
        {
            adjacentTiles.Add(null);
        }

        if (row < tiles.GetLength(0) - 1 && tiles[row + 1, column].tileType == TileType.Wall)
        {
            adjacentTiles.Add(tiles[row + 1, column]);
            wallCount++;
        }
        else
        {
            adjacentTiles.Add(null);
        }

        if (column > 0 && tiles[row, column - 1].tileType == TileType.Wall)
        {
            adjacentTiles.Add(tiles[row, column - 1]);
            wallCount++;
        }
        else
        {
            adjacentTiles.Add(null);
        }

        //TODO Calculate rotations
        switch (wallCount)
        {
            case 4:
                {
                    cellId += "_Cross";
                    break;
                }
            case 3:
                {
                    cellId += "_Three_Sided";
                    if (adjacentTiles[UP] == null) { rotation = 0; }
                    if (adjacentTiles[LEFT] == null) { rotation = 90; }
                    if (adjacentTiles[DOWN] == null) { rotation = 180; }
                    if (adjacentTiles[RIGHT] == null) { rotation = 270; }
                    break;
                }
            case 2:
                {
                    if (adjacentTiles[UP] != adjacentTiles[DOWN] && adjacentTiles[LEFT] != adjacentTiles[RIGHT])
                    {
                        cellId += "_Corner";
                        if (adjacentTiles[DOWN] != null && adjacentTiles[RIGHT] != null)       { rotation = 0;}
                        if (adjacentTiles[UP] != null && adjacentTiles[RIGHT] != null)     { rotation = 90;}
                        if (adjacentTiles[UP] != null && adjacentTiles[LEFT] != null)      { rotation = 180;}
                        if (adjacentTiles[DOWN] != null && adjacentTiles[LEFT] != null)        { rotation = 270;}
                    }
                    else
                    {
                        cellId += "_Normal";
                        if (adjacentTiles[UP] == null) 
                        {
                            rotation = 90;
                        }
                    }
                    break;
                }
            case 1:
                {
                    cellId += "_Half_Wall";
                    break;
                }
            default:
                {
                    UnityEngine.Debug.LogError("Unsupported number of walls");
                    break;
                }
        }

        return new Pair<string, int>(cellId, rotation);
    }

    public static Map LoadMapFromFile(string fileName) 
    {

        Map map;
        string path = string.Format("{0}/Resources/Maps/TextFiles/{1}.txt", Application.dataPath, fileName);
        int columns = -1;

        StreamReader reader = new StreamReader(path);
        string line = null;
        List<string> elements = new List<string>();
        while ((line = reader.ReadLine()) != null)
        {
            if (columns == -1)
            {
                columns = line.Length;
            }

            foreach (var character in line)
            {
                elements.Add(character.ToString());
            }
        }
        reader.Close();

        int rows = elements.Count / columns;
        map = new Map(rows, columns);
        int i = 0;
        foreach (var element in elements)
        {
            Tile.TileType type = Tile.GetTileTypeFromCharacter(element);
            map.tiles[i / columns, i % columns].tileType = type;
            if (type == TileType.Start) 
            {
                map.startTilePosition = new Pair<int, int>(i / columns, i % columns);
            }

            i++;
        }

        return map;
    }

}
