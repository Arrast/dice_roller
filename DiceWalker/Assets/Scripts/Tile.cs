﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public enum TileType
    {
        None = 0,
        Start = 1,
        Goal = 2,
        Wall = 3,
        Floor = 4,
        Enemy = 5,
        Pit = 6,
        Spikes = 7
    }

    public TileType tileType = TileType.None;
    public int row;
    public int column;

    public static TileType GetNextTileType(TileType tileType)
    {
        int max = Enum.GetValues(typeof(TileType)).Length;
        tileType = (TileType)(((int)tileType + 1) % max);

        return tileType;
    }

    public static string GetTileTypeFirstCharacter(TileType tileType)
    {
        string fc = tileType.ToString().Substring(0,1);
        return fc;
    }

    public static TileType GetTileTypeFromCharacter(string character) 
    {
        TileType tileType = TileType.None;
        var enums = Enum.GetValues(typeof(TileType)) as TileType[];
        foreach(var tileTypeEnum in enums)
        {
            if (tileTypeEnum.ToString().StartsWith(character)) 
            {
                tileType = tileTypeEnum;
                break;
            }
        }

        return tileType;
    }

}
