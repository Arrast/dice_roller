﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public int Health = 3;
    public int[] Dice = new int[3];
    public int Locked = -1;
    public int PositionX;
    public int PositionY;

    public Player() 
    {
        RollAllDice();
    }

    public void RollDice(int position) 
    {
        if (position >= 0 && position < Dice.Length && position != Locked) 
        {
            Dice[position] = Random.Range(1, 7);
        }
    }

    public void RollAllDice() 
    {
        for (int i = 0; i < Dice.Length; i++) 
        {
            RollDice(i);
        }
    }

    public override string ToString()
    {
        return $"I have {Health} Life. My Position is {PositionX},{PositionY} and my Rolls are {Dice[0]}, {Dice[1]}, {Dice[2]}";
    }
}
