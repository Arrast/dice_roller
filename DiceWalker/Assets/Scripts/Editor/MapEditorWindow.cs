﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using UnityEditor;
using UnityEngine;

public class MapEditorWindow : EditorWindow
{
    private float BUTTON_SIZE = 20;

    // Add menu named "My Window" to the Window menu
    [MenuItem("UGS/Create Map")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MapEditorWindow window = (MapEditorWindow)EditorWindow.GetWindow(typeof(MapEditorWindow));
        window.Show();
    }

    string fileName = "testFile";
    int numberOfRows = 0;
    int numberOfColumns = 0;

    Map map = null;

    private void OnGUI()
    {
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();

        numberOfRows = EditorGUILayout.IntField("Rows", numberOfRows);
        numberOfColumns = EditorGUILayout.IntField("Columns", numberOfColumns);
        if (GUILayout.Button("Generate"))
        {
            Generate();
        }

        if (GUILayout.Button("Save"))
        {
            Save();
        }

        if (GUILayout.Button("Build"))
        {
            Build();
        }

        if (GUILayout.Button("Load"))
        {
            Load();
        }

        fileName = GUILayout.TextField(fileName);
        GUILayout.EndHorizontal();
        GUILayout.Space(15);
        DrawButtons();
        GUILayout.EndVertical();

    }

    private void DrawButtons()
    {
        if (map != null)
        {
            GUILayout.BeginVertical();
            for (int i = 0; i < map.tiles.GetLength(0); i++)
            {
                GUILayout.BeginHorizontal();
                for (int j = 0; j < map.tiles.GetLength(1); j++)
                {
                    DrawButton(i, j);
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
    }

    private void DrawButton(int i, int j)
    {
        string tileIcon = Tile.GetTileTypeFirstCharacter(map.tiles[i, j].tileType);
        if (GUILayout.Button(tileIcon, GUILayout.Width(BUTTON_SIZE), GUILayout.Height(BUTTON_SIZE)))
        {
            map.tiles[i, j].tileType = Tile.GetNextTileType(map.tiles[i, j].tileType);
        }
    }

    private void Generate()
    {
        if (numberOfColumns > 0 && numberOfRows > 0)
        {
            map = new Map(numberOfRows, numberOfColumns);
        }
    }

    private void Load() 
    {
        map = Map.LoadMapFromFile(fileName);
    }

    private void Save()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < map.tiles.GetLength(0); i++)
        {
            for (int j = 0; j < map.tiles.GetLength(1); j++)
            {
                stringBuilder.Append(Tile.GetTileTypeFirstCharacter(map.tiles[i, j].tileType));
            }
            stringBuilder.AppendLine();
        }

        string path = string.Format("{0}/Resources/Maps/TextFiles/{1}.txt", Application.dataPath, fileName);
        StreamWriter writer = new StreamWriter(path, false);
        writer.Write(stringBuilder.ToString());
        writer.Close();
    }

    private void Build()
    {
        string path = string.Format("Assets/Resources/Maps/Prefabs/{0}.prefab", fileName);
        Dictionary<string, GameObject> prefabReferences = new Dictionary<string, GameObject>();
        GameObject go = new GameObject(fileName);
        foreach (Tile tile in map.tiles)
        {
            Pair<string, int> prefabAndRotation = map.GetTileIdByTileType(tile.row, tile.column, tile.tileType);
            if (prefabAndRotation != null)
            {
                UnityEngine.Debug.LogError($"{prefabAndRotation.first}");

                if (!prefabReferences.ContainsKey(prefabAndRotation.first))
                {
                     prefabReferences.Add(prefabAndRotation.first, PrefabUtility.LoadPrefabContents(string.Format("Assets/Resources/Tiles/{0}.prefab", prefabAndRotation.first)));
                }

                GameObject prefab = prefabReferences[prefabAndRotation.first];                
                if (prefab != null)
                {
                   GameObject instance = (GameObject) PrefabUtility.InstantiateAttachedAsset(prefab);
                    instance.name = $"[{tile.column},{tile.row}]";

                    VTile visualTile = instance.GetComponent<VTile>();
                    if (visualTile != null) 
                    {
                        visualTile.Init(prefabAndRotation.second);
                    }
                    instance.transform.SetParent(go.transform);
                    instance.transform.localPosition = new Vector3(tile.column, -tile.row);
                }
            }
        }

        PrefabUtility.SaveAsPrefabAsset(go, path);
        DestroyImmediate(go);
    }
}
