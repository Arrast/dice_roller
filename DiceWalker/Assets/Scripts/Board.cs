﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board
{
    public Map Map;
    public Player Player;

    public void StartGame(string map) 
    {
        Map = Map.LoadMapFromFile(map);
        Player = new Player();
        Player.PositionX = Map.startTilePosition.first;
        Player.PositionY = Map.startTilePosition.second;
    }
}
